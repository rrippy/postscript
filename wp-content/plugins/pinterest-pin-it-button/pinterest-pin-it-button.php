<?php
/*
  Plugin Name: Pinterest "Pin It" Button Lite
  Plugin URI: http://pinterestplugin.com
  Description: Add a Pinterest "Pin It" Button to your posts and pages allowing your readers easily pin your images. Includes shortcode and widget.
  Author: Phil Derksen
  Author URI: http://pinterestplugin.com
  Version: 1.4.1
  License: GPLv2
  Copyright 2012 Phil Derksen (phil@pinterestplugin.com)
*/

define( 'PIB_IS_PRO', false );

require_once( 'pib-config.php' );
