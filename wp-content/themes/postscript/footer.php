		<?php
		    // Use this hook to do things above the footer
		    notesblog_above_footer();

		    // The widgets are in sidebar-footer.php
		    get_sidebar('footer');
		?>
			
		</div></div></div><!-- /#main_content -->
        
		
		<div id="copy">
		    <div id="footer_info" class="fullcolumn">
		    	<p>Copyright &copy; 2012 <?php bloginfo('name'); ?>. &bull; Images and content may not be used without permission. &bull;
                <a href="<?php echo home_url(); ?>/terms/">Terms</a> &bull; <a href="<?php echo home_url(); ?>/privacy/">Privacy</a> &bull; 
                A <a href="http://www.novel.is" target="_blank">NOVEL</a> production.</p>
		    </div>
		</div>

</div><!-- /#site -->

<?php
    // Use this hook to do things below the site
    notesblog_below_site();
    
    // WordPress ends
    wp_footer();
?>
</body>
</html>