<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>" />

<meta name="viewport" content="width=1170" />

<script src="<?php bloginfo('template_url'); ?>/js/jquery-1.4.min.js"></script>
<script src="<?php bloginfo('template_url'); ?>/js/script.js"></script>

<!-- Google analytics -->
<script type="text/javascript">

  var _gaq = _gaq || [];
  _gaq.push(['_setAccount', 'UA-36298608-1']);
  _gaq.push(['_setDomainName', 'postscriptpost.com']);
  _gaq.push(['_trackPageview']);

  (function() {
    var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
    ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
    var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
  })();

</script>

<title>
	<?php 
		// Print the page title
		if (is_home () ) { 
			bloginfo('name'); 
		} elseif (is_category() || is_tag()) { 
			single_cat_title(); echo ' &bull; ' ; bloginfo('name'); 
		} elseif (is_single() || is_page()) { 
			single_post_title(); echo ' &bull; ' ; bloginfo('name');
		} else { 
			wp_title('',true); echo ' &bull; ' ; bloginfo('name');
		}
	?>
</title>
<link rel="profile" href="http://gmpg.org/xfn/11" />
<link rel="stylesheet" type="text/css" media="all" href="<?php bloginfo( 'stylesheet_url' ); ?>" />
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>" />

<link rel="shortcut icon" type="image/png" href="<?php bloginfo('template_url'); ?>/favicon.ico">

<?php
	// Comment JavaScript
	if ( is_singular() ) wp_enqueue_script( 'comment-reply' );

	//  Use this hook to insert things into HEAD
	notesblog_inside_head();
	
	// Kick off WordPress
	wp_head();
?>
</head>

<body <?php body_class(); ?>>
<div id="site">

	<?php
	    // Use this hook to insert things before the wrap
	    notesblog_above_site();
	?>

	<div id="outer-wrap"><div id="inner-wrap">
		<div id="header">
		</div>
		<div id="main_content" class="fullcolumn">