$(document).ready(function(){

  $('.wpsc_buy_button_container input').css({
    cursor: "pointer"
  });
   $('#comments #submit').css({
    cursor: "pointer"
  });
  $('.email-signup #mc-embedded-subscribe').css({
    cursor: "pointer"
  });
  $('#checkout_page_container .submit').css({
    cursor: "pointer"
  });
  $('#checkout_page_container .wpsc_buy_button').css({
    cursor: "pointer"
  });

  $('li.postcards_btn ul.sub-menu').css({
	display: "none"
  });
  
  var slide_button_number = $('#slide_buttons a').length;
   $('#slide_buttons').css({
		width: (slide_button_number*20) + 'px'
	});
	
	$('a.slide_btn_1').css('background-color', '#67B68D');

	
	
	// start timed slideshow
  $(function(){
	  setTimeout(function(){
		autoSlideshow();
	  }, 4000)
  });
  
  // price numbers to superscript
  $('span.currentprice').each(function (i) {
	 $(this).html($(this).html().replace('.00','<span class="superscript_numbers">00</span>')); 
  });
  // display posters submenu, except on mobile
  if (screen.width > 480) {
	  $('li.postcards_btn a').hover(function() {
		 $('li.postcards_btn > a').html('Postcards &#8212; ');
		 $('li.postcards_btn ul.sub-menu').css({
			display: "block"
		  });
	  // mouseout
	  }, function () {
		  $('li.postcards_btn > a').text('Postcards');
		  $('li.postcards_btn ul.sub-menu').css({
			display: "none"
		  });
	  });
  };
  
  // hover image for purchase options button
  $('.purachase_set a').hover(function() {
	  $('.purachase_set a .add_icon').css({
		'background-position': "top right"
	  });
  // mouseout
  }, function () {
	  $('.purachase_set a .add_icon').css({
		'background-position': "top left"
	  });
  });
  $('.purachase_print a').hover(function() {
	  $('.purachase_print a .add_icon').css({
		'background-position': "top right"
	  });
  // mouseout
  }, function () {
	  $('.purachase_print a .add_icon').css({
		'background-position': "top left"
	  });
  });
  
  // hover for cart widget number
  $('div#top-navigation div ul li.cart_btn a').hover(function() {
	  $('.cart_quantity').css({
		color: "#A87B4F"
	  });
  // mouseout
  }, function () {
	  $('.cart_quantity').css({
		color: "#333333"
	  });
  });
  
   // hover for cards on category pages
	$('.default_product_display > .imagecol').hover(function() {
	  $(this).find('.card_rollover').css({
		display: "block"
	  });
	  $(this).find('.weird-title').css({
		display: "none"
	  });
  // mouseout
  }, function () {
	  $(this).find('.card_rollover').css({
		display: "none"
	  });
  });
  
  // add RSS to social links
	$('<span><a href="http://postscriptpost.com/feed/rss/" target="_blank" class="mr_social_sharing_popup_link"><span class="mr_small_icon">RSS</span></a></span>')
	  .attr('class', 'mr_social_sharing')
	  .appendTo('.mr_social_sharing_wrapper');
	
	// slide show buttons
	$('a.slide_btn_1').click(function(e) {
		e.preventDefault();
		// clear autoslideshow
		$('#allSlides').removeClass('notselected');
		// button state
		$('#slide_buttons a').css('background-color', '#333');
		$(this).css('background-color', '#67B68D');
		// scroll to piece
		$('#allSlides').animate({'left':'0'}, 'slow');
   });
	
   $('a.slide_btn_2').click(function(e) {
		e.preventDefault();
		// clear autoslideshow
		$('#allSlides').removeClass('notselected');
		// button state
		$('#slide_buttons a').css('background-color', '#333');
		$(this).css('background-color', '#67B68D');
		// scroll to piece
		$('#allSlides').animate({'left':'-762'}, 'slow');
   });
   
    $('a.slide_btn_3').click(function(e) {
		e.preventDefault();
		// clear autoslideshow
		$('#allSlides').removeClass('notselected');
		// button state
		$('#slide_buttons a').css('background-color', '#333');
		$(this).css('background-color', '#67B68D');
		// scroll to piece
		$('#allSlides').animate({'left':'-1524'}, 'slow');
   });
   
   $('a.slide_btn_4').click(function(e) {
		e.preventDefault();
		// clear autoslideshow
		$('#allSlides').removeClass('notselected');
		// button state
		$('#slide_buttons a').css('background-color', '#333');
		$(this).css('background-color', '#67B68D');
		// scroll to piece
		$('#allSlides').animate({'left':'-2286'}, 'slow');
   });
   
   $('a.slide_btn_5').click(function(e) {
		e.preventDefault();
		// clear autoslideshow
		$('#allSlides').removeClass('notselected');
		// button state
		$('#slide_buttons a').css('background-color', '#333');
		$(this).css('background-color', '#67B68D');
		// scroll to piece
		$('#allSlides').animate({'left':'-3048'}, 'slow');
   });
   
   $('a.slide_btn_6').click(function(e) {
		e.preventDefault();
		// clear autoslideshow
		$('#allSlides').removeClass('notselected');
		// button state
		$('#slide_buttons a').css('background-color', '#333');
		$(this).css('background-color', '#67B68D');
		// scroll to piece
		$('#allSlides').animate({'left':'-3810'}, 'slow');
   });
	
});






function autoSlideshow() {
	
	var slide_button_number = $('#slide_buttons a').length;

	if ($('.notselected').position().left < '-3438') {
			// button state
			$('#slide_buttons a').css('background-color', '#333');
			$('a.slide_btn_1').css('background-color', '#67B68D');
			// scroll to piece
			$('#allSlides').animate({'left':'0'}, 'slow');
	} else if ($('.notselected').position().left < '-2676') {
		if (slide_button_number > 5 ) {
			// button state
			$('#slide_buttons a').css('background-color', '#333');
			$('a.slide_btn_6').css('background-color', '#67B68D');
			// scroll to piece
			$('#allSlides').animate({'left':'-3810'}, 'slow');
		} else {
			// button state
			$('#slide_buttons a').css('background-color', '#333');
			$('a.slide_btn_1').css('background-color', '#67B68D');
			// scroll to piece
			$('#allSlides').animate({'left':'0'}, 'slow')
		}
	} else if ($('.notselected').position().left < '-1914') {
		if (slide_button_number > 4 ) {
			// button state
			$('#slide_buttons a').css('background-color', '#333');
			$('a.slide_btn_5').css('background-color', '#67B68D');
			// scroll to piece
			$('#allSlides').animate({'left':'-3048'}, 'slow');
		} else {
			// button state
			$('#slide_buttons a').css('background-color', '#333');
			$('a.slide_btn_1').css('background-color', '#67B68D');
			// scroll to piece
			$('#allSlides').animate({'left':'0'}, 'slow')
		}
	} else if ($('.notselected').position().left < '-1152') {
		if (slide_button_number > 3 ) {
			// button state
			$('#slide_buttons a').css('background-color', '#333');
			$('a.slide_btn_4').css('background-color', '#67B68D');
			// scroll to piece
			$('#allSlides').animate({'left':'-2286'}, 'slow');
		} else {
			// button state
			$('#slide_buttons a').css('background-color', '#333');
			$('a.slide_btn_1').css('background-color', '#67B68D');
			// scroll to piece
			$('#allSlides').animate({'left':'0'}, 'slow')
		}
	} else if ($('.notselected').position().left < '-390') {
			if (slide_button_number > 2 ) {
			// button state
			$('#slide_buttons a').css('background-color', '#333');
			$('a.slide_btn_3').css('background-color', '#67B68D');
			// scroll to piece
			$('#allSlides').animate({'left':'-1524'}, 'slow');
		} else {
			// button state
			$('#slide_buttons a').css('background-color', '#333');
			$('a.slide_btn_1').css('background-color', '#67B68D');
			// scroll to piece
			$('#allSlides').animate({'left':'0'}, 'slow')
		}
	} else if ($('.notselected').position().left < '372') {
		if (slide_button_number > 1 ) {
			// button state
			$('#slide_buttons a').css('background-color', '#333');
			$('a.slide_btn_2').css('background-color', '#67B68D');
			// scroll to piece
			$('#allSlides').animate({'left':'-762'}, 'slow');
		} else {
			// button state
			$('#slide_buttons a').css('background-color', '#333');
			$('a.slide_btn_1').css('background-color', '#67B68D');
			// scroll to piece
			$('#allSlides').animate({'left':'0'}, 'slow')
		}
	} 
	setTimeout(autoSlideshow, 5000);

}