<?php 
	// The basic loop for heading
	while ( have_posts() ) : the_post(); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php 
			// Use this hook to do things above below the page title
			notesblog_above_page_title_single();
		?>
		<h1 class="entry-title">
			&#8212; <?php the_title(); ?> &#8212;
		</h1>
        <div class="header_divit"></div>
		<?php 
			// Use this hook to do things above below the page title
			notesblog_below_page_title_single();
		?>
		<?php edit_post_link( __( 'Edit', 'notesblog' ), '<div class="entry-meta">', '</div>' ); ?>
<?php 
// End the loop
endwhile; ?>
	
    

	
        
<?php rewind_posts();
	// The loop for the blog
	query_posts('');
	while ( have_posts() ) : the_post(); ?>
	
	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<?php if ( has_post_format( 'aside' ) ) { ?>
		<div class="format-aside-permalink">
			<a href="<?php the_permalink();?>" title="<?php the_title_attribute(); ?>">&para;</a>
		</div>
		<?php the_content(); ?>
	<?php } else { ?>
		<?php 
			// Use this hook to do things between above the post title
			notesblog_above_post_title_listing();
		?>
		<h2 class="entry-title">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
				<?php the_title(); ?>
			</a>
		</h2>
		<?php 
			// Use this hook to do things between below the post title
			notesblog_below_post_title_listing();
		?>
				<div class="entry-content">
					<?php the_content(); ?>
					<?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'notesblog' ), 'after' => '</div>' ) ); ?>
                    
				</div>
		<?php  
			// Comments only on single posts
			if ( is_singular() ) {
				comments_template( '', true ); 
			}
		} ?>
	</div>
	<?php 
	// End the loop
	endwhile; ?>
	
	<?php
	// When possible, display navigation at the bottom
	if ( $wp_query->max_num_pages > 1 ) : ?>
	<div id="nav-below" class="navigation">
		<div class="nav-previous">
			<?php next_posts_link( __( '<span class="meta-nav">&larr;</span> Older posts', 'notesblog' ) ); ?>
		</div>
		<div class="nav-next">
			<?php previous_posts_link( __( 'Newer posts <span class="meta-nav">&rarr;</span>', 'notesblog' ) ); ?>
		</div>
	</div>
<?php endif; ?>