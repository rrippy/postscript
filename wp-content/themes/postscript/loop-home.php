<?php 
	// The basic loop to show the home page image
	$args = array(
			'post_type' => 'front-page-image',
			'posts_per_page' => 6
	); ?>




<div id="slideshow">
	
    <!-- slideshow images -->
	<div id="slides">
        <div id="allSlides" class="notselected">
	
		<?php $mainImage = get_posts($args);
        foreach ( $mainImage as $post ) {
           setup_postdata($post); ?>
           
                 <!-- remove links from image -->
                <?php
                  preg_match_all("/(<img [^>]*>)/",get_the_content(),$matches,PREG_PATTERN_ORDER);
                  for( $i=0; isset($matches[1]) && $i < count($matches[1]); $i++ ) {
                    echo $beforeEachImage . $matches[1][$i] . $afterEachImage;
                  } ?>
                        
        <?php } ?>
    
    	</div><!--close #allSlides-->
    </div><!--close #slides-->
    
    <!-- slideshow buttons -->
    <div id="slide_buttons">
    	<?php for( $i=0; $i < count($mainImage); $i++ ) { ?>
			<a href="#" class="slide_btn_<?php echo $i+1 ?>"></a> <?php ;
		} ?>
    </div><!--close #slide_buttons-->

</div><!--close #slideshow-->