<?php 
	// The basic loop
	while ( have_posts() ) : the_post(); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php 
			// Use this hook to do things above below the page title
			notesblog_above_page_title_single();
		?>
		<h1 class="entry-title">
			<?php if (wpsc_is_in_category('postcards')) { 
				echo ' &#8212; Postcards  &#8212;';
			} else { ?>
				 &#8212; <?php the_title(); ?> &#8212;
			<?php } ?>
		</h1>
        <div class="header_divit"></div>
		<?php 
			// Use this hook to do things above below the page title
			notesblog_below_page_title_single();
		?>
		<?php edit_post_link( __( 'Edit', 'notesblog' ), '<div class="entry-meta">', '</div>' ); ?>
		<div class="entry-content">
		    <?php the_content(); ?>
		    <?php wp_link_pages( array( 'before' => '<div class="page-link">' . __( 'Pages:', 'notesblog' ), 'after' => '</div>' ) ); ?>
		
        	<!-- if contact page show sign-up form -->
        	<?php if (is_page(40)) { ?>
			
            <div class="email-signup">
                <!-- Begin MailChimp Signup Form -->
                <div id="mc_embed_signup">
                <form action="http://novel.us6.list-manage.com/subscribe/post?u=a0fb937b4a77835b6a81f197a&amp;id=b0aceb05ef" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                    <label for="mce-EMAIL"></label>
                    <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
                    <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
                </form>
                </div>        
        	<!--End mc_embed_signup-->
            </div>
            
            <div class="twitter_follow">
            	<a href="http://www.twitter.com/PostscriptGoods/" target="_blank">Follow us on Twitter</a>
            </div>

		<?php } ?> <!-- close contact section -->


		</div>
	</div>

<?php 
	// End the loop
	endwhile; ?>