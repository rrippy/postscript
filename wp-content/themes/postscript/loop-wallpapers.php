<?php 
	// The basic loop for heading
	while ( have_posts() ) : the_post(); ?>

	<div id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
		<?php 
			// Use this hook to do things above below the page title
			notesblog_above_page_title_single();
		?>
		<h1 class="entry-title">
			&#8212; <?php the_title(); ?> &#8212;
		</h1>
        <div class="header_divit"></div>
		<?php 
			// Use this hook to do things below the page title
			notesblog_below_page_title_single();
		?>
		<?php edit_post_link( __( 'Edit', 'notesblog' ), '<div class="entry-meta">', '</div>' ); ?>
		<div class="entry-content">
		    <?php the_content(); ?>
		</div>
	</div>

<?php 
	// End the loop
	endwhile; ?>




<?php rewind_posts();
	// The loop for the wallpapers

	$args = array(
			'post_type' => 'wallpaper',
			'posts_per_page' => -1
	);
	
	$wall = get_posts($args);
	foreach ( $wall as $post ) {
	   setup_postdata($post); ?>
       
       <div class="entry-content">
	   	<?php the_content();?>
        <h2 class="entry-title">
			<a href="<?php the_permalink(); ?>" title="<?php the_title_attribute(); ?>" rel="bookmark">
				<?php the_title(); ?>
			</a>
		</h2>
		<?php 
			// Use this hook to do things between below the wallpaper title
			notesblog_below_wallpaper_title_listing();
		?>
	   </div>
        
<?php } ?>