<?php get_header(); ?>
<?php get_sidebar(); ?>

	<div id="content" class="widecolumn">
		<?php
			// Look for loop-index.php, fallback to loop.php
			get_template_part( 'loop', 'blog' );
		?>
	</div>

<?php get_footer(); ?>