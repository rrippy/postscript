<div id="sidebar-container" class="column">

    <div id="site-header">
        <a href="<?php echo home_url(); ?>" title="<?php bloginfo('name'); ?>">
            <img src="<?php bloginfo('template_url'); ?>/images/postscript-logo.png" />
        </a>
    </div>
            
	<div id="top-navigation">
		<?php wp_nav_menu('top-navigation'); ?>
        
        <div class="cart_quantity">
        <?php 
			// Adding widget area inside the header
			dynamic_sidebar('beside-the-logo');
		?>
        </div>
        
    </div>
    
</div>