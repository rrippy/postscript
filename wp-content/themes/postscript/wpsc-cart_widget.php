<?php if(wpsc_cart_item_count() > 0): ?>

    
		<?php while(wpsc_have_cart_items()): wpsc_the_cart_item(); ?>
			
			<?php do_action ( "wpsc_before_cart_widget_item_name" ); ?><?php do_action ( "wpsc_after_cart_widget_item_name" ); ?>
                    <form action="" method="post" class="adjustform">
					<input type="hidden" name="quantity" value="0" />
					<input type="hidden" name="key" value="<?php echo wpsc_the_cart_item_key(); ?>" />
					<input type="hidden" name="wpsc_update_quantity" value="true" />
				</form>	
		<?php endwhile; ?>


					<?php printf( _n(' (%d)', ' (%d)', wpsc_cart_item_count(), 'wpsc'), wpsc_cart_item_count() ); ?>


	
<?php endif; ?>

<?php
wpsc_google_checkout();
?>
