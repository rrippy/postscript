<?php
	/**
	 * The Transaction Results Theme.
	 *
	 * Displays everything within transaction results.  Hopefully much more useable than the previous implementation.
	 *
	 * @package WPSC
	 * @since WPSC 3.8
	 */

	global $purchase_log, $errorcode, $sessionid, $echo_to_screen, $cart, $message_html;
?>
<div class="wrap">

<?php
	echo wpsc_transaction_theme();
	if ( ( true === $echo_to_screen ) && ( $cart != null ) && ( $errorcode == 0 ) && ( $sessionid != null ) ) {			
		
		// Code to check whether transaction is processed, true if accepted false if pending or incomplete
		
		
		echo "<br />" . wpautop(str_replace("$",'\$',$message_html)); ?>
		
        <p class="email-sign-up">Be the first to know about new Postscript prints!</p>
		<div class="email-signup">
            <!-- Begin MailChimp Signup Form -->
            <div id="mc_embed_signup">
            <form action="http://novel.us6.list-manage.com/subscribe/post?u=a0fb937b4a77835b6a81f197a&amp;id=b0aceb05ef" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
                <label for="mce-EMAIL"></label>
                <input type="email" value="" name="EMAIL" class="email" id="mce-EMAIL" placeholder="email address" required>
                <input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button">
            </form>
            </div>        
        <!--End mc_embed_signup-->
        </div>
								
	<?php } elseif ( true === $echo_to_screen && ( !isset($purchase_log) ) ) {
			_e('There is nothing in your cart.', 'wpsc') . "<a href=".get_option("product_list_url").">" . __('Please visit our shop', 'wpsc') . "</a>";
	}
?>	
	
</div>